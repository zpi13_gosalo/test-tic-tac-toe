<?php
    session_start();
    require_once APP_ROOT.'/app/core/model.php';
    require_once APP_ROOT.'/app/core/view.php';
    require_once APP_ROOT.'/app/core/controller.php';
    require_once APP_ROOT.'/app/core/route.php';
    Route::start();
?>