<?php
    include APP_ROOT.'/app/entities/PlayRoom.php';

    class Controller_Game extends Controller {

        function __construct() {
            $this->model = new Model_Game();
            $this->view = new View();
        }

        function action_new() {
            if(!isset($_SESSION['room_id']) && isset($_SESSION['player_id'])) {
                $field = PlayRoom::createField(20);
                $room = $this->model->createRoom($field);

                $_SESSION['room_id'] = $room['id'];
                $_SESSION['sig'] = $room['sig'];

                echo $_SESSION['sig'];
            } else echo 'continue';
        }

        function action_find() {
            if(!isset($_SESSION['room_id']) && isset($_SESSION['player_id'])) {
                $room = $this->model->getFreeRoom();
                if($room) {
                    $sig = $this->model->joinRoom($room);
                    $_SESSION['room_id'] = $room;
                    $_SESSION['sig'] = $sig;
                    echo $_SESSION['sig'];
                } else echo 'finding';
            } else echo 'continue';
        }

        function action_step() {
            if(isset($_SESSION['room_id']) && isset($_SESSION['player_id'])) {
                $room_id = $_SESSION['room_id'];
                $sig = $_SESSION['sig'];

                $x = $_POST['x'];
                $y = $_POST['y'];

                $room = $this->model->getRoom($room_id);
                $play_room = new PlayRoom(20, 5, $room['field']);
                $play_room->step($sig, $x, $y);  
                $this->model->saveRoom($room_id, $play_room->getField(), null, $sig); 
            }
        }

        function action_checkres() {
            if(isset($_SESSION['room_id']) && isset($_SESSION['player_id'])) { 
                $room_id = $_SESSION['room_id'];
                $sig = $_SESSION['sig'];

                $room = $this->model->getRoom($room_id);
                $play_room = new PlayRoom(20, 5, $room['field']);
                $res = $play_room->checkWinSig($sig);

                if($res != 'continue') {
                    switch($res) {
                        case 'win': 
                        $this->model->setPlayerInfo('wins', $_SESSION['player_id']); 
                        $_SESSION['player_wins'] += 1; 
                        break;
                        case 'lose': 
                        $this->model->setPlayerInfo('loses', $_SESSION['player_id']); 
                        $_SESSION['player_loses'] += 1; 
                        break;
                        case 'draw': 
                        $this->model->setPlayerInfo('draws', $_SESSION['player_id']); 
                        $_SESSION['player_draws'] += 1;
                    }
                    $this->model->saveRoom($room_id, null, 'closed');
                    unset($_SESSION['room_id']);
                    unset($_SESSION['sig']);
                } else $this->model->saveRoom($room_id, null, 'playing');

                echo $res;
            }
        }

        function action_checkoponent() {
            if(isset($_SESSION['room_id']) && isset($_SESSION['player_id'])) {
                $id = $_SESSION['room_id'];
                $room = $this->model->getRoom($id);

                if($room['status'] == 'playing') echo ($_SESSION['sig'] =='x')? 'o' : 'x';
                else echo 'waiting';
            }
        }

        function action_info() {
            if(isset($_SESSION['room_id']) && isset($_SESSION['player_id'])) { 
                $info['sig'] = $_SESSION['sig'];
               // $info['oponent_name'] = 
            }
        }

        function action_getfield() {
            if(isset($_SESSION['room_id']) && isset($_SESSION['player_id'])) {
                $id = $_SESSION['room_id'];
                $room = $this->model->getRoom($id);
                echo $room['field'];
            } else echo "empty";   
        }
    }
?>