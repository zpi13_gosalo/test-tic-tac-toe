<?php
    class Controller_Login extends Controller
    {
        function __construct()
        {
            $this->model = new Model_Login();
            $this->view = new View();
        }

        function action_index()
        {
            if(isset($_POST['login']) && isset($_POST['password'])) {
                $res = $this->model->login($_POST['login'], $_POST['password']);
                
                if(!$res) {
                    session_destroy();
                    header('Content-Type: text/html; charset=utf-8'); 
                    echo "<script>alert('Некорректные учётные данные!'); window.location = '/login';</script>";
                } 
                else {
                    $_SESSION['player_id'] = $res['id'];
                    $_SESSION['player_name'] = $res['username'];
                    $_SESSION['player_wins'] = $res['wins'];
                    $_SESSION['player_loses'] = $res['loses'];
                    $_SESSION['player_draws'] = $res['draws'];
                    header("Location: /"); 
                    die(); 
                }
            } elseif(!isset($_SESSION['player_id'])) $this->view->generate('login_view.php', 'template_view.php', 'Вход');
            else header("Location: /");
        }

        function action_exit() {
            session_destroy();
        }
    }
?>