<?php
    class Controller_Register extends Controller
    {
        function __construct()
        {
            $this->model = new Model_Register();
            $this->view = new View();
        }

        function action_index()
        {
            if(isset($_POST['login']) && isset($_POST['password']) && isset($_POST['retype'])) {

                $res = $this->model->register($_POST['login'], $_POST['password'], $_POST['retype']);
                header('Content-Type: text/html; charset=utf-8'); 
                if($res) echo "<script>alert('Произошла ошибка. Проверьте правильность введенных данных.'); window.location = '/register';</script>";
                else {
                    header("Location: /login"); 
                    die(); 
                }
            } else $this->view->generate('register_view.php', 'template_view.php', 'Регистрация');
        }
    }
?>