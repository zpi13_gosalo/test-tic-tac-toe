<?php
 class Model
 {
    public static function db_connect()
    {
        require APP_ROOT.'/app/config.php';

        $connection = new mysqli($servername, $username, $password, $dbname);
        
        if ($connection->connect_error) {
             die("Connection failed: " . $conn->connect_error);
         }
         return $connection;
    }
    
     public function get_data()
     {
     }

     protected function qu($str, $conn) {
        return $conn->real_escape_string($str);
    }
 }
?>