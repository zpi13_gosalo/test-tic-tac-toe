<?
class PlayRoom {
    private $field; //поле
    private $size; //размер поля
    private $draw_sum; //сумма ничьей
    private $win_len;

    public static function createField($n) {
        return array_fill(0, $n, array_fill(0, $n, 'e'));
    }

    // проверка выиграшных линий
    private function checkWin($c) {
        $f_size = $this->size;
        $len = $this->win_len;
        for ($i = 0; $i < $f_size; $i++) { // проход по всему полю
            for ($j = 0; $j < $f_size; $j++) {
                if($this->checkLine($i, $j, 1, 0, $len, $c)) return true;   // проверка линии по х
                if($this->checkLine($i, $j, 1, 1, $len, $c)) return true;   // проверка по диагонали х у
                if($this->checkLine($i, $j, 0, 1, $len, $c)) return true;   // проверка линии по у
                if($this->checkLine($i, $j, 1, -1, $len, $c)) return true;  // проверка по диагонали х -у
            }
        }
        return false;
    }

    // проверка линии
    private function checkLine($x, $y, $vx, $vy, $len, $c) {
        $far_x = $x + ($len - 1) * $vx;  // подсчет конца проверяемой линии
        $far_y = $y + ($len - 1) * $vy;

        if (!isset($this->field[$far_x][$far_y])) return false; // проверка выхода проверяемой линии за пределы поля

        for ($i = 0; $i < $len; $i++) {                 // проход по проверяемой линии
            if ($this->field[$y + $i * $vy][$x + $i * $vx] != $c) return false;   // проверка ячеек
        }
        return true;
    }

    public function __construct($size, $win_len, $field) {

        $this->size = $size; //размер поля
        $this->win_len = $win_len; //длина выиграшной комбинации

        $this->field = json_decode($field);   //загрузить поле
    
        $half_count = floor(($this->size * $this->size) / 2); //расчитать сумму ничьей
        $this->draw_sum = ($half_count * 1) + ($half_count * 2);
        
    }

    public function getField() { //получить поле для отрисовки
        return json_encode($this->field);
    }

    public function step($sig, $x, $y) { //сделать ход
        if($this->field[$x][$y] == 'e') $this->field[$x][$y] = $sig;
    }

    public function checkWinSig($sig) { 

        $sig_rev = ($sig =='x')? 'o': 'x';

        if($this->checkWin($sig)) return "win"; //проверить на выиграш
        if($this->checkWin($sig_rev)) return "lose"; //проверить на проиграш

        $sum = 0;
        array_walk_recursive($this->field, 
        function($val) use (&$sum) { $sum += $val; }); //проверить на ничью
        
        if($this->draw_sum == $sum) return "draw";
        return "continue";
    }
}
