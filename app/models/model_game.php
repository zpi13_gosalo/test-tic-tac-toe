<?php 

class Model_Game extends Model {
    private $conn;

    public function __construct() {
        $this->conn = Model::db_connect();
        $this->conn->set_charset("UTF-8");
    }

    public function createRoom($field) {
        $j_field = json_encode($field);

        $sig = (mt_rand(0, 100) >= 50)? 'x' : 'o';
        $sql = "INSERT INTO `rooms`(`field`, `status`, `sig`) VALUES ('$j_field', 'free', '$sig')";
        $this->conn->query($sql);
        $res['id'] = $this->conn->insert_id;
        $res['sig'] = $sig;
        return $res;
    }

    public function joinRoom($room) {
        $sql = "UPDATE `rooms` SET `status`='playing' WHERE `id`='$room'";
        $this->conn->query($sql);

        $sql = "SELECT `sig` FROM `rooms` WHERE `id` = '$room'";
        $sig = $this->conn->query($sql)->fetch_assoc()['sig'];

        return ($sig =='x')? 'o' : 'x';
    }

    public function getFreeRoom() {
        $sql = "SELECT * FROM `rooms` WHERE `status` = 'free' ORDER BY `id` ASC LIMIT 1";
        $res = $this->conn->query($sql);

        return ($res)? $res->fetch_assoc()['id'] : false;
    }

    public function getRoom($room) {
        $sql = "SELECT * FROM `rooms` WHERE `id` = '$room'";
        $res = $this->conn->query($sql);

        return $res->fetch_assoc();
    }

    public function setPlayerInfo($type, $player) {
        "UPDATE `users` SET `$type`='$type' + 1 WHERE `id`='$player'";
    }

    public function saveRoom($room, $field, $status, $sig) {

        if ($status != null) {
            $sql = "UPDATE `rooms` SET `status`='$status' WHERE `id`='$room'";
            $this->conn->query($sql);
        }

        if ($field != null) {
            $sql = "UPDATE `rooms` SET `field`='$field' WHERE `id`='$room'";
            $this->conn->query($sql);
        }
        
        if ($sig != null) {
            $sql = "UPDATE `rooms` SET `sig`='$sig' WHERE `id`='$room'";
            $this->conn->query($sql);
        }        
    }

}

?>