<form method="POST" action="/login/" class="ui-form">
  <h3>Войти в игру!</h3>
   <div class="form-row">
    <input type="text" name="login" id="email" required autocomplete="off"><label for="email">Логин</label>
  </div>
  <div class="form-row">
    <input type="password" name="password" id="password" required autocomplete="off"><label for="password">Пароль</label>
  </div>
  <p><input type="submit" value="Войти"></p>
  <p align="center" style="margin-top: 10px"><a style="color: #4a90e2;" href="/register">Регистрация</a></p>
</form>