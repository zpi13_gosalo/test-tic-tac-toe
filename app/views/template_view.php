<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?=$page_title?></title>
        <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <link href='styles/style.css' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <?php include APP_ROOT.'/app/views/'.$content_view; ?>
        <footer>
        </footer>     
    </body>
</html>