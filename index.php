<?php 
    ini_set('display_errors', 1);
    ini_set('session.gc_maxlifetime', 3000);
    ini_set('session.cookie_lifetime', 3000);

    define('APP_ROOT', __DIR__);
    require_once APP_ROOT.'/app/bootstrap.php';
?>