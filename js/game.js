function start() {
    $.ajax({
    url: "game/getfield",
    method: "POST",
    success: function(data) {
        if(data != 'empty') {
        setInterval(function(){
            getField();
        }, 200);
    }
}   
});
}


function getField() {
    $.ajax({
        url: "game/getfield",
        method: "POST",
        success: function(data) { 
            if(data != 'empty') {
                field = JSON.parse(data);
                for(i=0; i<20; i++)
                    for(j=0; j<20; j++) {
                        if(field[i][j] == 'x')
                            $('#'+i+'-'+j+'.cell').css('background', 'url(styles/img/cr.png)');
                        else if(field[i][j] == 'o')
                            $('#'+i+'-'+j+'.cell').css('background', 'url(styles/img/no.png)');
                        else $('#'+i+'-'+j+'.cell').css('background', 'white');
                }

                //$('#msg').html('<p style="text-align: center; color: #0099ff; ">Комната создана! Ожидание противника...</p>');
            }
         }
    });
}

function logout() {
    $.ajax({
        url: "login/exit",
        method: "POST",
        success: function(data){
            window.location = '/';
        }
    });
}

var checkOp = 0;
function newRoom() {
    $('#btnFind').prop("disabled", true);
    $('#btnNew').prop("disabled", true);
    $.ajax({
        url: "game/new",
        method: "POST",
        success: function(data){
            $('#msg').html('<p style="text-align: center; color: #0099ff; ">Комната создана! Ожидание противника...</p>');
            $('#player1_sig').html(data);
            sig = (data =='x')? 'o' : 'x';
            $('#player2_sig').html(sig);
            checkOp = setInterval(function(){
                $.ajax({
                    url: "game/checkoponent",
                    method: "POST",
                    success: function(data) {
                        if(data != 'waiting') {
                            $('#msg').html('<p style="text-align: center; color: #0099ff; ">Игра началась! Крестик ходит первым.</p>');
                            clearInterval(checkOp);
                            start();
                        }
                    }
                });
            }, 500);
            
        }
    });
}

var wait = 0;
function findRoom() {
    $('#btnNew').prop("disabled", true);
    $('#btnFind').prop("disabled", true);
    $('#msg').html('<p style="text-align: center; color: #0099ff; ">Поиск свободной комнаты...</p>');
    wait = setInterval(function(){
    $.ajax({
        url: "game/find",
        method: "POST",
        success: function(data) {
            if(data != 'finding' && data != 'continue') {
                $('#msg').html('<p style="text-align: center; color: #0099ff;">Игра началась! Крестик ходит первым.</p>');
                clearInterval(wait);
                sig = (data =='x')? 'o' : 'x';
                $('#player1_sig').html(data);
                $('#player2_sig').html(sig);
                start();
            }
        }
    });
    }, 500);
}

$(function(){
    start();

    $('.cell').click(function() {
        id = $(this).attr('id');
        var sp = id.split('-');
        $.ajax({
            url: "game/step",
            method: "POST",
            data: { x: sp[0], y: sp[1] },
            success: function() {
                getField();
            }
        });
    });
})

